import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class DataTypesDemo{
    private int age;//0
    private String name;//null
    private float radio1;//0.0f
    private boolean myFlag;//false
    public static void main(String[] args) throws IOException{
        //primitivos
        byte mes = 12;// 1 byte
        short year=2002;//2 byte
        int x = 3999;
        long y = 33333;
        float radio = 3.3999f;
        double diametro = 44.54444;
        char sexo = 'M';//2 byte
        boolean flag = false;

        //Object
        String name = "Pepito";//12 byte

        name = "Ana";//6 byte

        System.out.format("radio: %.2f\n",radio);

        Scanner scan = new Scanner(System.in);

        System.out.print("Digite edad: ");
        mes = scan.nextByte();
        System.out.print("Digite radio: ");
        radio = scan.nextFloat();
        System.out.print("Digite sexo: ");
        sexo = scan.next().charAt(0);//"M" o "F"
        
        System.out.println("Su edad: " + mes);
        System.out.println("Su radio: " + radio);
        System.out.println("Su sexo: " + sexo);

        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Digite edad: ");
        mes = Byte.parseByte(buffer.readLine());
        System.out.print("Digite radio: ");
        radio = Float.parseFloat(buffer.readLine());
        System.out.println("Su edad: " + mes);
        System.out.println("Su radio: " + radio);
    }
}

/*Numericos
Enteros:
    byte        1 byte
    short       2 byte
    int         4 byte
    long        8 byte
Decimales:
    float       4 byte  
    double      8 byte

No numericos:
    char        2 byte
    boolean     ?
    String      depende de la cantidad de caracteres        */